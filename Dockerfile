FROM gcr.io/projectsigstore/cosign:v1.11.1 as COSIGN
FROM registry.gitlab.com/av1o/helm-install-image:v3-1.25

COPY src/ build/

USER root

ARG ALPINE_MIRROR="https://alpine.global.ssl.fastly.net/alpine"
ARG ALPINE_VERSION="3.16"
# Install Dependencies
RUN apk update --no-cache \
	    -X "$ALPINE_MIRROR/v${ALPINE_VERSION}/main" && \
	apk upgrade --no-cache \
    	-X "$ALPINE_MIRROR/v${ALPINE_VERSION}/main" && \
	apk add --no-cache openssl curl tar gzip bash jq \
    	-X "$ALPINE_MIRROR/v${ALPINE_VERSION}/main"

RUN ln -s /build/bin/* /usr/local/bin/

RUN chown 1001:0 /usr/local/bin/auto-deploy
COPY --from=COSIGN /ko-app/cosign /usr/local/bin

USER somebody
WORKDIR /home/somebody
